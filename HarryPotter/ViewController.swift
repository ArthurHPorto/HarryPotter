//
//  ViewController.swift
//  HarryPotter
//
//  Created by Arthur Porto on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Actor: Decodable{
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var actorList:[Actor]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actorList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let actor = actorList![indexPath.row]
        
        cell.Name.text = actor.name
        cell.ActorName.text = actor.actor
        cell.ActorImage.kf.setImage(with: URL(string: actor.image))
        
        return cell
    }
    
    @IBOutlet var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        data()
        TableView.dataSource = self
    }
    
    private func data(){
        AF.request("https://hp-api.herokuapp.com/api/characters")
            .responseDecodable(of: [Actor].self){
            response in
                if let actor_list = response.value{
                    self.actorList = actor_list
            }
                self.TableView.reloadData()
        }
    }
}

