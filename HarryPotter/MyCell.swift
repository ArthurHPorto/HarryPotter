//
//  MyCell.swift
//  HarryPotter
//
//  Created by Arthur Porto on 20/10/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet var Name: UILabel!
    @IBOutlet var ActorName: UILabel!
    @IBOutlet var ActorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
